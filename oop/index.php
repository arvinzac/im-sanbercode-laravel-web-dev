<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("Shaun");
echo "Nama Hewan: ".$sheep->get_name()."<br>"; // "shaun"
echo "Jumlah Kaki: ".$sheep->get_legs()."<br>"; // 4
echo "Berdarah Dingin: ".$sheep->get_cold_blooded()."<br>"; // "no"

$kodok = new Frog("Buduk");
echo "<br> Nama Hewan: ".$kodok->get_name()."<br>"; 
echo "Jumlah Kaki: ".$kodok->get_legs()."<br>";
echo "Berdarah Dingin: ".$kodok->get_cold_blooded()."<br>";
echo "Jump: ".$kodok->jump()."<br>"; // "hop hop"

$sungokong = new Ape("Kera Sakti");
echo "<br> Nama Hewan: ".$sungokong->get_name()."<br>";
echo "Jumlah Kaki: ".$sungokong->get_legs()."<br>";
echo "Berdarah Dingin: ".$sungokong->get_cold_blooded()."<br>";
echo "Yell: ".$sungokong->yell(); // "Auooo"
?>