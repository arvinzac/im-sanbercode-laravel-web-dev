<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth(Request $request)
    {
        $first = $request->firstname;
        $last = $request->lastname;

        return view('page.submit', ['ND'=>$first, 'NB'=>$last]);
    }
}
