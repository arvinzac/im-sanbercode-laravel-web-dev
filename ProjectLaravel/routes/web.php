<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Homecontroller;
use App\Http\Controllers\Authcontroller;
use App\Http\Controllers\Castcontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/register', [HomeController::class,'form']);
Route::post('/welcome', [AuthController::class,'auth']);
Route::get('/table', function () {
    return view('page.table');
});
Route::get('/data-table', function () {
    return view('page.data-table');
});

//CREATE
Route::get('/cast/create',  [CastController::class,'create']);
Route::post('/cast',  [CastController::class,'store']);

//READ
Route::get('/cast',  [CastController::class,'index']);

//DETAIL
Route::get('/cast/{id}',  [CastController::class,'show']);

//UPDATE
Route::get('/cast/{id}/edit',  [CastController::class,'edit']);
Route::put('/cast/{id}',  [CastController::class,'update']);

//DELETE
Route::delete('/cast/{id}',  [CastController::class,'destroy']);