@extends('layouts.master')
@section('title')
Halaman Form
@endsection
@section('sub_title')
halaman form
@endsection
@section('content')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label><br><br>
        <select>
            <option value="indo">Indonesian</option>
            <option value="fore">Singapura</option>
            <option value="fore">Malaysia</option>
            <option value="fore">Thailand</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="lang">Bahasa Indonesia<br>
        <input type="checkbox" name="lang">English<br>
        <input type="checkbox" name="lang">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="message"  cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection