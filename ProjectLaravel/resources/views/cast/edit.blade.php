@extends('layouts.master')
@section('title')
Halaman Edit Cast
@endsection
@section('sub_title')
halaman cast
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
      <label>Edit Nama Cast</label>
      <input type="text" value="{{$cast->nama}}" class="form-control"" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
        <label>Edit Umur Cast</label>
        <input type="text" value="{{$cast->umur}}" class="form-control"" name="umur">
      </div>
      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
      <div class="form-group">
        <label>Edit Biodata Cast</label>
        <textarea class="form-control" name="bio" rows="3">{{$cast->bio}}</textarea>
      </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-primary" >Kembali</a>
  </form>

@endsection