@extends('layouts.master')
@section('title')
Halaman Detail Cast
@endsection
@section('sub_title')
halaman cast
@endsection
@section('content')
<h1>{{$cast->nama}}</h1>
<h2>{{$cast->umur}}</h2>
<p>{{$cast->bio}}</p>
<a href="/cast" class="btn btn-primary btn-sm my-2" >Kembali</a>
@endsection