@extends('layouts.master')
@section('title')
Halaman Tambah Cast
@endsection
@section('sub_title')
halaman cast
@endsection
@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" class="form-control"" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
        <label>Umur Cast</label>
        <input type="text" class="form-control"" name="umur">
      </div>
      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
      <div class="form-group">
        <label>Biodata Cast</label>
        <textarea class="form-control" rows="3" name="bio"></textarea>
      </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-primary" >Kembali</a>
  </form>

@endsection